{ name = "my-project"
, dependencies =
    [ "aff"
    , "effect"
    , "payload"
    , "prelude"
    ]
, packages = ./packages.dhall
, sources =
    [ "src/**/*.purs"
    -- , "test/**/*.purs"
    ]
}
